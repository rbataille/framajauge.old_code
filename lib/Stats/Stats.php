<?php
/**
 * Framajauge
 * Copyright (C) 2014 Framajauge team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * See /LICENCE for more information
 * @contact framajauge@framalistes.com
 */

/**
 *  This class store all user stats
 */
class Stats{
    private $id; // User id
    private $last_access; // Store last access to stats file
    private $time; // Time spent in jauge
    private $tt_rt; // Twitter retweet bool
    private $fb_share; // Facebook share bool
    private $fb_like; // Facebook like bool

    /**
     *  Basic constructor
     */
    public function __construct($__data = false){
        if(is_array($__data)){
            $this->Hydrate($__data);
        }
        else{
            $this->Reset();
        }
    }
    
    /**
     *  Return an empty Stats class
     */
    static public function EmptyStats(){
        $class_name = __CLASS__;
        $object = new $class_name();
        $object->Reset();
        return $object;
    }

    /**
     * transform this class into array
     */
    public function ToArray(){
        $obj_array = get_object_vars($this);
        $final = array();
        foreach($obj_array as $key => $value){
            $final[$key] = $value;
        }
        return $final;
    }

    /**
     * transform this class into string
     */
    public function ToStr(){
        $obj_array = get_object_vars($this);
        $final = array();
        foreach($obj_array as $key => $value){
            array_push($final, $key."=".trim($value));
        }
        return implode($final, PHP_EOL);
    }
    
    /**
     *  User ID accessor
     */
    public function GetId(){
        return $this->id;
    }
    public function SetId($__id){
        $this->id = $__id;
    }

    /**
     *  Time accessor
     */
    public function GetTime(){
        return $this->time;
    }
    public function SetTime($__time){
        $this->time = $__time;
    }

    /**
     *  Last Access accessor
     */
    public function GetLastAccess(){
        return $this->last_access;
    }
    public function SetLastAccess(){
        $this->last_access = time();
    }

    /**
     *  Twetter retwet accessor
     */
    public function GetTtRt(){
        return $this->tt_rt;
    }
    public function SetTtRt($__tt_rt){
        $this->tt_rt = $__tt_rt;
    }

    /**
     *  Facebook share accessor
     */
    public function GetFbShare(){
        return $this->fb_share;
    }
    public function SetFbShare(bool $__fb_share){
        $this->fb_share = $__fb_share;
    }

    /**
     *  Facebook like accessor
     */
    public function GetFbLike(){
        return $this->fb_like;
    }
    public function SetFbLike(bool $__fb_like){
        $this->fb_like = $__fb_like;
    }

    /**
     *  Set all values with $__input array
     */
    public function Hydrate($__input){
        if(is_array($__input)){
            foreach($__input as $key => $value){
                if(property_exists(__CLASS__, $key)){
                    $this->$key = $value;
                }
            }
        }        
    }

    public function Update($__input){
        if(is_array($__input)){
     
        }
        $old_last_access = $this->GetLastAccess();
        $this->SetLastAccess();
        $new_time = $this->GetTime() + ($this->GetLastAccess() - $old_last_access);
        if($old_last_access == 0){
            $this->SetTime(0);
        }
        else{
            $this->SetTime($new_time);
        }
    }
 
    /**
     *  Reset all stats fields
     */
    private function Reset(){
        $proplist = get_object_vars($this);
        foreach($proplist as $key => $value){
            $this->$key = 0;
        }
    }
}
