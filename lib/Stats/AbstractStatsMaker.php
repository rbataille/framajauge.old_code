<?php
/**
 * Framajauge
 * Copyright (C) 2014 Framajauge team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * See /LICENCE for more information
 * @contact framajauge@framalistes.com
 */


abstract class AbstractStatsMaker{
    abstract static public function Get($__session_id, array $__entry);
    abstract static public function Set($__session_id, Stats $__input);
    /**
     *  return an array of key/value extracted from $__data array
     *  
     */
    protected function FormatRawData(array $__data, $__separator = '='){
        $output = array();
        foreach($__data as $value){
            if(strpos($value, $__separator) !== false){
                $line_parts = explode($__separator, $value, 2);
                if(count($line_parts) >= 2){
                    $output[$line_parts[0]] = $line_parts[1];
                }
            }
        }
        return $output;
    }

}