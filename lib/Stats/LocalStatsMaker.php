<?php
/**
 * Framajauge
 * Copyright (C) 2014 Framajauge team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * See /LICENCE for more information
 * @contact framajauge@framalistes.com
 */

require_once dirname(__FILE__) . DS . "AbstractStatsMaker.php";

class LocalStatsMaker extends AbstractStatsMaker{

    static public function GetAll(){
        $output = array();
        if($dir_handle = opendir(self::GetContainer())) {
            while (false !== ($entry = readdir($dir_handle))) {
                if(!is_dir($entry)){
                    $session_id = str_replace(".data", "", $entry);
                    array_push($output, LocalStatsMaker::Get($session_id, array()));
                }
            }
        }
        return $output;
    }

    /**
     *  Return Stats object with all data stored localy for user with session id = $__session_id
     *  If data's not found, create new file and return empty Stats object 
     */
    static public function Get($__session_id, array $__entry){
        $stats = false;
        $filename = self::GetContainer($__session_id);
        // if stats file for user found, update it
        $content = @file($filename);
        if($content !== false){
            $data = parent::FormatRawData($content);
            $stats = new Stats($data);
        }
        // else create new
        else{
            $stats = Stats::EmptyStats();
            $stats->Hydrate($__entry);
            $stats->SetId($__session_id);
            self::Set($__session_id, $stats);
        }
        return $stats;
    }

    /**
     * Save stats $__input_stats to local file
     */
    static public function Set($__session_id, Stats $__input_stats){
        $filename = self::GetContainer($__session_id);
        file_put_contents($filename, $__input_stats->ToStr());
    }

    /**
     *  Return location of container file for user $__session_id
     */
    private function GetContainer($__session_id = false){
        if($__session_id===false){
            return "./data/";
        }
        else{
            return "./data/".$__session_id.".data";
        }
    }
}
