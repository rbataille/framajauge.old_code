<?php
/**
 * Framajauge
 * Copyright (C) 2014 Framajauge team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * See /LICENCE for more information
 * @contact framajauge@framalist.com
 */

require_once dirname(__FILE__) . DS . "FileStatWriter.php";

class WriterFactory{
  private $writer_list;
  public function __construct(){
    array_push($this->writer_list, "FileStatWriter");
  }

  public function GetWriter($__name){
    if(array_key_exist($__name, $this->writer_list)){
      return new $__name();
    }
    else{
      throw new Exception("no writer found!");
    }
  }

}
