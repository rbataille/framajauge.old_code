<?php
/**
 * Framajauge
 * Copyright (C) 2014 Framajauge team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * See /LICENCE for more information
 * @contact framajauge@framalistes.com
 */

require_once dirname(__FILE__) . DS . "IStatWriter.php";

class FileStatWriter implements IStatWriter{
    private $filename;

    public function __construct($__params){
        if(array_key_exists("filename", $__params)){
            $this->filename = $__params["filename"];
        }      	           	     
    }

    public function Write($__data){
        file_put_contents($this->filename, $__data);
    }
}
