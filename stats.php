<?php
/**
 * Framajauge
 * Copyright (C) 2014 Framajauge team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * See /LICENCE for more information
 * @contact framajauge@framalistes.com
 */

session_start();

define("DS", DIRECTORY_SEPARATOR);

include(dirname(__FILE__) . DS . "lib" . DS . "Template/LocalTemplate.php");
include(dirname(__FILE__) . DS . "lib" . DS . "Writer/WriterFactory.php");
include(dirname(__FILE__) . DS . "lib" . DS . "Stats/Stats.php");
include(dirname(__FILE__) . DS . "lib" . DS . "Stats/LocalStatsMaker.php");

$all_data = LocalStatsMaker::GetAll();
$sess_id = session_id();
$current_stats = LocalStatsMaker::Get($sess_id, $_POST);
$needed = array("time"=>function($value){
    $time_spent = gmdate("H:i:s", $value);
    return "Temps pass&eacute; sur la page=$time_spent";
});

echo ToHtml($current_stats->ToArray(), $needed, $all_data);


function ToHtml(array $__data, array $__filtered, $__all){
    $output = "<table>";
    foreach($__data as $key => $value){
        if(array_key_exists($key, $__filtered)){
            $output .= "<tr><td>".$__filtered[$key]($value)."</td></tr>";
        }
    }
    $output .= "<tr><td>".MergeTime($__all)."</td></tr>";
    $output .= "</table>";

    return $output;
}

function MergeTime(array $__stats_array){
    $output_time = 0;
    foreach($__stats_array as $value){
        $output_time += $value->GetTime();
    }
    $output_time = gmdate("H:i:s", $output_time);
    return "Total (tout utilisateur) =$output_time";
}