Framajauge
==========

La FramaJauge est un plugin WordPress qui permet d’afficher, sous forme de jauge, l’intérêt des internautes pour une page ou un article, et d’utiliser cette information pour activer de nouvelles publications. C’est une illustration graphique de l’économie de l’attention, la valeur qu’un internaute crée sur un site par ses visites, sa présence, son utilisation, ses commentaires et ses partages.

Retrouvez nous sur :

Une liste de diffusion : http://www.framalistes.org/sympa/subscribe/framajauge sur laquelle vous pouvez échanger pour contribuer au projet.
Un pad (éditeur de texte collaboratif) présente en détail le projet et les réflexions fonctionnelles et/ou techniques notamment : https://lite.framapad.org/p/FramaJauge
Un dépôt git : https://git.framasoft.org/rbataille/framajauge
