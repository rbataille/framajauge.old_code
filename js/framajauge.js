/**
 * Framajauge
 * Copyright (C) 2014 Framajauge team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * See /LICENCE for more information
 * @contact framajauge@framalistes.org
 */

var Utils = {
    is_def : function(Item){
	if(typeof Item !== "undefined")
	    return true;
	return false;
    }
};

/** 
 * class that create DOM elements from JSON
 */
var DOM_forger = {
    from_JSON : function($parent, JSON){
	for(var i = 0; i < JSON.length; ++i){
	    var $element = $("<div></div>");
	    var $current_item = JSON[i].node;
	    if(Utils.is_def($current_item.name))
		$element.attr("id", $current_item.name);

	    if(Utils.is_def($current_item.style))
		$element = this.import_style($element, $current_item.style);

	    if(Utils.is_def($current_item.childs))
		this.from_JSON($element, $current_item.childs);

	    if(Utils.is_def($current_item.content))
		$element.html($current_item.content);
	    
	    if(Utils.is_def($current_item.mouseenter)){
		$element.on("mouseenter", function(){
		    $current_item.mouseenter();
		});
	    }
	    if(Utils.is_def($current_item.mouseleave)){
		$element.on("mouseleave", function(){
		    $current_item.mouseleave();
		});
	    }
	    $element.appendTo($parent);
	}
    },
    import_style : function(node, style){
	for(var it = 0; it < style.length; ++it){
	    node.css(style[it]);
	}
	return node;
    }
};

/** 
 * Create the Jauge itself from JSON data.
 * If jauge already present, content was replaced, 
 * else the jauge was created
 */ 
var Framajauge = {
    bind: function(){
	var self = this;
	var stats_bt = $("#stats-button");
	if(stats_bt.length){
	    stats_bt.on("click", function(){
		self.get_stats();
	    });
	}
    },
    create: function(JSON){
	var hash = JSON.hash;
	var jauge = JSON.jauge;
	if(!$("#framajauge").length){
	    var node = $('<div id="framajauge" value="'+hash+'"></div>');
	    node.appendTo("body");
	}
	else{
	    if($("#framjauge").val() !== hash){
		this.clear_content();
	    }
	}
	DOM_forger.from_JSON($("#framajauge"), jauge);
	Framajauge.bind();
    },
    clear_content : function(){
	$("#framajauge").children().each(function(){
	    this.remove();
	});
    },
    get_stats : function(){
	console.log("in get_stats");
	function render_stats(received){
	    $("#jauge-stats").html(received);
	}
	requester.query("stats.php", "self",  render_stats);
    }
};

/**
 *  Ajax requester
 */
var requester = {
    query : function(url, param, callback){
	$.ajax({
	    url:url,
	    data:param,
	    async:true,
	    type:"post",
	    success:function(received){
		callback(received);
	    },
	    // @TODO => do something on error
	    error:function(a, b, c){
		console.log(a);
		console.log(b);
		console.log(c);
	    }
	});
    }
};

/**
 * main function, run when page is fully loaded
 */
$(document).ready(function(){
    function update_function(){
	requester.query("ajax.php", "template_id=001", Framajauge.create);
	setTimeout(function(){
	    update_function();
	}, 30000);
    };
    update_function();
});




