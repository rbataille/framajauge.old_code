<?php
/**
 * Framajauge
 * Copyright (C) 2014 Framajauge team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * See /LICENCE for more information
 * @contact framajauge@framalistes.com
 */

session_start();

define("DS", DIRECTORY_SEPARATOR);

include(dirname(__FILE__) . DS . "lib" . DS . "Template/LocalTemplate.php");
include(dirname(__FILE__) . DS . "lib" . DS . "Writer/WriterFactory.php");
include(dirname(__FILE__) . DS . "lib" . DS . "Stats/Stats.php");
include(dirname(__FILE__) . DS . "lib" . DS . "Stats/LocalStatsMaker.php");

$sess_id = session_id();
$current_stats = LocalStatsMaker::Get($sess_id, $_POST);
$current_stats->Update($_POST);
LocalStatsMaker::Set($sess_id, $current_stats);

$values = ["percent"=>"45", 
           "margin-top"=>"37"];
$template = new LocalTemplate($values);
$template->Content();
